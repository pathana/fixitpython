from modules import driver as sd
from selenium.webdriver.common.keys import Keys
import requests

driver = None
cen_id = "1468"
rep_id = "4767"
fixit_cen_id = "10"
fix_date = "2019-07-17"

def get_service():
    driver.center()
    driver.repair(cen_id)
    # driver.service(get_repid(fix_date))
    driver.service(rep_id)

def get_repid(startdate):
    value = {
        "2019-06-16": "3067",
        "2019-06-15": "3066",
        "2019-06-14": "3066",
        "2019-06-13": "3066",
        "2019-06-22": "3351",
        "2019-06-23": "3352",
        "2019-06-29": "3710",
        "2019-06-30": "3711",
        "2019-07-07": "3988",
        "2019-07-06": "3987",
    }

    return value[startdate]

def address(address):
    split = address.split(' ')
    new_address = ''

    for s in split:
        if (s == ' '): continue
        if (s == '-'): continue
        new_address += s

    return new_address

def insert(repair, index):
    citizen = repair['citizen']
    item = repair['item']
    item_type = item['item_type']
    start_date = repair['created_at'].split(' ')[0]
    start_time = repair['created_at'].split(' ')[1]

    driver.find_id("ser_idcard").send_keys(citizen['number'])
    driver.find_id("ser_name").send_keys(citizen['title'] + citizen['fullname'])
    driver.find_id("ser_address").send_keys(address(citizen['address']))
    driver.find_id("ser_code").send_keys(index + 1)
    driver.find_id("ser_time").send_keys(start_time)
    driver.find_id("ser_phone").send_keys(citizen['phone'])
    startdate = 'document.getElementById("ser_startdate").value = arguments[0];'
    driver.driver.execute_script(startdate, start_date)
    select = driver.select_name("equ_id")
    select.select_by_value(item_type['code'])

    driver.find_id("rep_cause").send_keys(item['problem'])
    driver.find_id("rep_performance").send_keys(repair['description'])

    select = driver.select_name("ser_result")
    select.select_by_value("ซ่อมได้")

    select = driver.select_name("ser_standby")
    select.select_by_value("รับแล้ว")

    driver.find_id("ser_idcard").send_keys(Keys.ENTER)
    driver.wait_alert()

def sendback():
    repairs = requests.get('http://fixitchainat.ddns.net:8000/api/repairs/waitsendback/' + fixit_cen_id)
    return repairs.json()['data']

def finished():
    repairs = requests.get('http://fixitchainat.ddns.net:8000/api/repairs/finished/' + fixit_cen_id)
    return repairs.json()['data']

def repairs():
    repairs = requests.get('http://fixitchainat.ddns.net:8000/api/repairs/notwait/' + fixit_cen_id + '/' + fix_date)
    return repairs.json()['data']

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    repairs = repairs()
    # print(len(repairs))

    for i in range(33, len(repairs)):
        repair = repairs[i]
        start_date = repair['created_at'].split(' ')[0]
        get_service()
        insert(repair, i)
