import json
from modules import driver as sd
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from datetime import datetime

driver = None
cen_id = "820"

def openfile(name):
    with open(name, 'r', encoding='utf8') as f:
        data = json.load(f)

    return data

def get_service():
    driver.center()
    driver.repair(cen_id)

def parse_date_dash(dt):
    return datetime.strptime(dt, '%Y-%m-%d')

def parse_date_slash(dt):
    return datetime.strptime(dt, '%d/%m/%Y')

def get_repairmember_links():
    get_service()

    rep_keys = ['id', 'startdate']
    links = []

    for i in range(1, 197):
        data = {}
        for key in rep_keys:
            rep_name = 'rep_' + key + str(i)
            value = driver.find_name(rep_name).get_attribute("value")
            data[key] = value

        links.append(data)

    return links

def find_member(member_citizen):
    tr_list = driver.driver.find_elements_by_tag_name("tr")

    for i in range(1, len(tr_list)):
        td_list = tr_list[i].find_elements_by_tag_name("td")
        citizen_number = td_list[2].text
        if (str(citizen_number) == str(member_citizen)): return True

    return False


def add_repair_member(personal, links):
    p_startdate = parse_date_slash(personal['startdate'])
    p_enddate = parse_date_slash(personal['enddate'])

    for link in links:
        dt = parse_date_dash(link['startdate'])
        if (p_startdate > dt or p_enddate < dt): continue

        driver.repairmember(link['id'])
        if (find_member(personal['citizen_number'])): continue

        select = driver.select_name("cmem_id")
        select.select_by_value(str(personal['id']))
        form = driver.find_name("form1")
        form.submit()

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    links = get_repairmember_links()

    term1_61 = openfile('student_61.json')

    for i in range(0, len(term1_61)):
        add_repair_member(term1_61[i], links)
        print(i)
