import pandas as pd
from modules import driver as sd
from selenium.webdriver.common.keys import Keys
import requests

driver = None
cen_id = "820"

def get_service():
    driver.center()
    driver.repair(cen_id)

def add_date(date_format):
    rep_startdate = 'document.getElementById("rep_startdate").value = arguments[0];'
    driver.driver.execute_script(rep_startdate, date_format)
    driver.find_id("rep_startdate").send_keys(Keys.ENTER)
    driver.wait_alert()

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    get_service()

    term1 = pd.date_range(start="13/5/2019", end="13/9/2019")
    # term2 = pd.date_range(start="8/10/2018", end="15/2/2019")

    for d in term1:
        weekday = int(d.strftime("%w"))
        if (weekday < 2): continue

        date_format = d.strftime("%Y-%m-%d")
        add_date(date_format)
