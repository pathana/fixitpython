import requests

if __name__ == "__main__":
    r = requests.get('http://localhost/api/officer_all')
    officers = r.json()['data']

    for officer in officers:
        print(officer['department']['name'])
