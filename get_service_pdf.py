from modules import driver as sd
from selenium.webdriver.common.keys import Keys
import requests
from datetime import datetime
import json
import csv

driver = None
cen_id = "820"

def get_service():
    driver.center()
    driver.repair(cen_id)

def parse_date_dash(dt):
    return datetime.strptime(dt, '%Y-%m-%d')

def parse_date_slash(dt):
    return datetime.strptime(dt, '%d/%m/%Y')

def get_repair_data():
    get_service()

    rep_keys = ['id', 'startdate']
    datas = []

    tr_list = driver.driver.find_elements_by_tag_name("tr")

    for i in range(1, len(tr_list) - 1):
        data = {}
        for key in rep_keys:
            rep_name = 'rep_' + key + str(i)
            value = driver.find_name(rep_name).get_attribute("value")
            data[key] = value

        datas.append(data)

    return datas

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    repair_data = get_repair_data()

    print_startdate = parse_date_dash("2018-05-15")
    print_stopdate = parse_date_dash("2019-02-15")

    for data in repair_data:
        rep_startdate = parse_date_dash(data['startdate'])
        if (print_startdate > rep_startdate or print_stopdate < rep_startdate): continue
        driver.print_service_pdf(data['id'], data['startdate'])
