import os
import PyPDF2

dir_location = "C:/Users/lenovo-v530/Documents/printrepair"
os.chdir(dir_location)

pdf2merge = []

for filename in os.listdir('.'):
    if filename.endswith('.pdf'):
        pdf2merge.append(filename)

pdfWriter = PyPDF2.PdfFileWriter()

for filename in pdf2merge:
    pdfFileObj = open(filename, 'rb')
    pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

    for pageNum in range(pdfReader.numPages):
        pageObj = pdfReader.getPage(pageNum)
        pdfWriter.addPage(pageObj)

pdfOutput = open("repair-final.pdf", 'wb')
pdfWriter.write(pdfOutput)
pdfOutput.close()
