import json
import requests

def repairs(fixit_cen_id, fixit_date):
    repairs = requests.get('http://fixitchainat.ddns.net:8000/api/repairs/notwait/' + fixit_cen_id + '/' + fixit_date)
    return repairs.json()['data']

if __name__ == "__main__":
    cen_list = [
        {'id': '9', 'date': '2019-07-17'},
        {'id': '9', 'date': '2019-07-16'},
        {'id': '10', 'date': '2019-07-17'},
        {'id': '10', 'date': '2019-07-16'},
        {'id': '8', 'date': '2019-07-14'},
        {'id': '8', 'date': '2019-07-13'},
        {'id': '7', 'date': '2019-07-14'},
        {'id': '7', 'date': '2019-07-13'},
        {'id': '6', 'date': '2019-07-07'},
        {'id': '6', 'date': '2019-07-06'},
        {'id': '5', 'date': '2019-07-07'},
        {'id': '5', 'date': '2019-07-06'},
    ]

    repair_list = []

    for cen in cen_list:
        repair_list += repairs(cen['id'], cen['date'])

    with open('repairs.json', 'w') as json_file:
        json.dump(repair_list, json_file)
