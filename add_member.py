from modules import driver as sd
from selenium.webdriver.common.keys import Keys
import requests
import json

driver = None
member_url = "http://reward.svc.ac.th/fixit62/centerdetail.php?3klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb5&action=center_member&3klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb5"

def openfile(name):
    with open(name, 'r', encoding='utf8') as f:
        data = json.load(f)

    return data

def insert(officer):
    driver.find_id("cmem_idcard").send_keys(officer['citizen_number'])
    driver.find_id("cmem_name").send_keys(officer['fullname'])
    driver.find_id("cmem_depart").send_keys(officer['dept'])
    driver.find_id("cmem_phone").send_keys('-')
    select = driver.select_name("gro_id")
    # type = type_value(officer['type'])
    type = type_value('teacher')
    select.select_by_value(type)
    driver.find_id("cmem_idcard").send_keys(Keys.ENTER)
    driver.wait_alert()

def officers():
    r = requests.get('http://fixitchainat.ddns.net:8000/api/officer_all')
    return r.json()['data']

def type_value(name):
    value = {
        'teacher': '1',
        'student': '2',
        'communitiy': '3',
        'officer': '4',
        'chief': '5'
    }
    return value[name]

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    driver.get(member_url)

    officers = openfile('personal.json')

    for officer in officers:
        insert(officer)
