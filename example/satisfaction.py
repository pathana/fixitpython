from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import pandas as pd
import sys

driver = None

def find_id(name):
    return driver.find_element_by_id(name)

def seleniums(driver, citizens):
    for index, citizen in citizens.iterrows():
        form(driver, citizen)

def form(driver, citizen):
    driver.get('http://vecrsa.vec.go.th/user/form_satisfaction.php')
    find_id('ServiceRef').send_keys(citizen['service_ref'])
    select = Select(driver.find_element_by_name('DateSatasfaction'))
    select.select_by_value(pd.Timestamp(citizen['date_check']).strftime('%Y-%m-%d'))

    gender = 'document.getElementById("' + gender_id(citizen['gender']) + '").checked = true;'
    driver.execute_script(gender)

    education = 'document.getElementById("' + education_id(citizen['education']) + '").checked = true;'
    driver.execute_script(education)

    career = 'document.getElementById("' + career_id(citizen['career']) + '").checked = true;'
    driver.execute_script(career)

    news_fillform(driver, citizen['news'])
    services_fillform(driver, citizen['services'])

    cars = driver.find_elements_by_name("CarCome")

    for element in cars:
        if element.get_attribute("value") == car_value(citizen['car_type']):
            driver.execute_script("arguments[0].id = 'CarCome1';", element)
            driver.execute_script("arguments[0].checked = true;", element)

    satisfaction_value = citizen['satisfaction.satisfaction']

    satisfaction = 'document.getElementById("' + satisfaction_id(satisfaction_value) + '").checked = true;'
    driver.execute_script(satisfaction)

    carcheck = 'document.getElementById("' + carcheck_id(citizen['car_check']) + '").checked = true;'
    driver.execute_script(carcheck)

    # if len(citizen['suggestion']) > 0:
    #     find_id('Suggestion').send_keys(citizen['suggestion'])

    find_id('btnSubmit').click()

    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_matches('http://vecrsa.vec.go.th/user/form_satisfaction_total.php'))
    return

def satisfaction_id(name):
    id = {
        "5": "Satisfacion1",
        "4": "Satisfacion2",
        "3": "Satisfacion3",
        "2": "Satisfacion4",
        "1": "Satisfacion5",
    }
    return id[str(name)]

def carcheck_id(name):
    id = {
        "5": "CarCheck1",
        "4": "CarCheck2",
        "3": "CarCheck3",
        "2": "CarCheck4",
        "1": "CarCheck5",
    }
    return id[str(name)]

def car_value(name):
    value = {
        'pickup': 'รถกระบะ / รถปิกอัพ',
        'car': 'รถยนต์ / รถเก๋ง',
        'van': 'รถตู้ / รถแวน',
        'motocycle': 'รถจักรยานยนต์',
        'other': 'อื่น ๆ'
    }
    return value[name]

def news_fillform(driver, news):
    news = news.replace("[","").replace("]","")
    list = news.split(',')

    for new in list:
        element = 'document.getElementById('+ new + ').checked = true;'
        driver.execute_script(element)

    return

def services_fillform(driver, services):
    services = services.replace("[","").replace("]","")
    list = services.split(',')

    for service in list:
        element = 'document.getElementById('+ service + ').checked = true;'
        driver.execute_script(element)

    return

def gender_id(name):
    id = {
        "ชาย": "Gender1",
        "หญิง": "Gender2"
    }
    return id[name]

def education_id(name):
    id = {
        "ต่ำกว่าปริญญาตรี": "EducationalBackground1",
        "ปริญญาตรี": "EducationalBackground1",
        "สูงกว่าปริญญาตรี": "EducationalBackground1"
    }
    return id[name]

def career_id(name):
    id = {
        'ข้าราชการ': 'Career1',
        'พนักงานรัฐวิสาหกิจ': 'Career2',
        'พนักงานบริษัทเอกชน': 'Career3',
        'ประกอบอาชีพส่วนตัว': 'Career4',
        'อื่น ๆ': 'Career5'
    }
    return id[name]

def read_csv(filename):
    return pd.read_csv(filename)

def login(driver):
    driver.get('http://vecrsa.vec.go.th/login/login.php')
    find_id("username").send_keys("1318016101")
    find_id("password").send_keys("fixitchainat")
    find_id("login").click()
    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_matches('http://vecrsa.vec.go.th/user/user_index.php'))

if __name__ == "__main__":
    citizens = read_csv(str(sys.argv[1]))
    driver = webdriver.Chrome()
    login(driver)
    seleniums(driver, citizens)
