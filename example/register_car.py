from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import pandas as pd
import sys
import json
import numpy as np

driver = None

def find_id(name):
    return driver.find_element_by_id(name)

def read_csv(filename):
    return pd.read_csv(filename)

def seleniums(driver, citizens):
    for index, citizen in citizens.iterrows():
        if citizen['car.car_type'] == 'motocycle': motocycle(driver, citizen)
        else: car(driver, citizen)

def motocycle(driver, citizen):
    driver.get('http://vecrsa.vec.go.th/user/form_motorcycle.php')
    find_id('ServiceRef').send_keys(citizen['service_ref'])
    find_id('Prefix').send_keys(citizen['prefix'])
    find_id('FirstName').send_keys(citizen['first_name'])
    find_id('LastName').send_keys(citizen['last_name'])
    find_id('IndentificationNumber').send_keys(citizen['id_number'])
    find_id('Telephone').send_keys(citizen['telephone'])
    find_id('CarCode').send_keys(citizen['car.car_code'])
    find_id('CarProvince').send_keys(citizen['car.car_province'])
    find_id('CarBrand').send_keys(citizen['car.car_brand'])
    find_id('Insurance').send_keys(citizen['car.insurance'])
    select = Select(driver.find_element_by_name('DateCheck'))
    select.select_by_value(pd.Timestamp(citizen['date_check']).strftime('%Y-%m-%d'))
    maintenance_fillform(driver, citizen['maintenance.options'])
    if (isinstance(citizen['maintenance.suggestions'], str)):
        driver.execute_script('document.getElementById("Suggestions").value = arguments[0]', citizen['maintenance.suggestions'])
    find_id('btnSubmit').click()
    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_matches('http://vecrsa.vec.go.th/user/form_motorcycle_total.php'))

def car(driver, citizen):
    driver.get('http://vecrsa.vec.go.th/user/form_car.php')
    find_id('ServiceRef').send_keys(citizen['service_ref'])
    find_id('Prefix').send_keys(citizen['prefix'])
    find_id('FirstName').send_keys(citizen['first_name'])
    find_id('LastName').send_keys(citizen['last_name'])
    find_id('IndentificationNumber').send_keys(citizen['id_number'])
    find_id('Telephone').send_keys(citizen['telephone'])
    find_id('CarCode').send_keys(citizen['car.car_code'])
    find_id('CarProvince').send_keys(citizen['car.car_province'])
    find_id('CarBrand').send_keys(citizen['car.car_brand'])
    find_id('Insurance').send_keys(citizen['car.insurance'])
    select = Select(driver.find_element_by_name('DateCheck'))
    select.select_by_value(pd.Timestamp(citizen['date_check']).strftime('%Y-%m-%d'))
    url = 'document.getElementById("CarType").value = "' + car_type(citizen['car.car_type']) + '";'
    driver.execute_script(url)
    maintenance_fillform(driver, citizen['maintenance.options'])
    if (isinstance(citizen['maintenance.suggestions'], str)):
        driver.execute_script('document.getElementById("Suggestions").value = arguments[0]', citizen['maintenance.suggestions'])
    find_id('btnSubmit').click()
    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_matches('http://vecrsa.vec.go.th/user/form_car_total.php'))

def maintenance_fillform(driver, options):
    json_options = json.loads(options)

    for key, value in json_options.items():
        elements = driver.find_elements_by_name(key)
        for element in elements:
            if element.get_attribute("value") == "0":
                driver.execute_script("arguments[0].checked = true;", element)

        detail_key = key + 'Detail'
        element = driver.find_element_by_name(detail_key)
        element.send_keys(value)

    return

def car_type(name):
    type = {
        "pickup": "รถกระบะ / รถปิกอัพ",
        "car": "รถยนต์ / รถเก๋ง",
        "van": 'รถตู้ / รถแวน',
        "other": 'อื่น ๆ'
    }
    return type[name]

def login(driver):
    driver.get('http://vecrsa.vec.go.th/login/login.php')
    find_id("username").send_keys("1318016101")
    find_id("password").send_keys("fixitchainat")
    find_id("login").click()
    wait = WebDriverWait(driver, 10)
    wait.until(EC.url_matches('http://vecrsa.vec.go.th/user/user_index.php'))


if __name__ == "__main__":
    citizens = read_csv(str(sys.argv[1]))
    driver = webdriver.Chrome()
    login(driver)
    seleniums(driver, citizens)
