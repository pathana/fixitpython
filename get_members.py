from modules import driver as sd
from selenium.webdriver.common.keys import Keys
import requests
import json
import csv

driver = None
member_url = "http://reward.svc.ac.th/fixit62/centerdetail.php?3klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb5&action=center_member&3klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb53klnl3cpsovse4ehr8r4mmceb5"

def get_member():
    members = []
    cmem_keys = ['id', 'idcard', 'name', 'depart']

    tr_list = driver.driver.find_elements_by_tag_name("tr")

    for i in range(1, len(tr_list) - 1):
        member = []
        td_list = tr_list[i].find_elements_by_tag_name("td")
        for key in cmem_keys:
            cmem_name = 'cmem_' + key + str(i)
            value = driver.find_name(cmem_name).get_attribute("value")
            member.append(value)

        member.append(td_list[1].text)
        members.append(member)

    return members

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    driver.get(member_url)
    members = get_member()

    with open('members.csv', 'w', encoding='utf-8') as csvFile:
        writer = csv.writer(csvFile)
        writer.writerows(members)

    csvFile.close()
