import json
from modules import driver as sd
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests
import random
from datetime import datetime

driver = None
citizens = None
repairs = None
cen_id = "820"

def openfile(name):
    with open(name, 'r', encoding='utf8') as f:
        data = json.load(f)

    return data

def get_service():
    driver.center()
    driver.repair(cen_id)

def address(address):
    split = address.split(' ')
    new_address = ''

    for s in split:
        if (s == ' '): continue
        if (s == '-'): continue
        new_address += s

    return new_address

def get_service_links():
    get_service()
    tr_list = driver.driver.find_elements_by_tag_name("tr")
    insert_links = []

    for tr in tr_list:
        td_list = tr.find_elements_by_tag_name("td")
        if (len(td_list) != 9): continue

        citizen_count = int(td_list[2].text)
        if (citizen_count >= 1): continue

        service_link = tr.find_element(By.PARTIAL_LINK_TEXT, "ออกใบรับซ่อม")
        insert_links.append(service_link.get_attribute("href"))

    return insert_links

def parse_date(dt):
    return datetime.strptime(dt, '%Y-%m-%d')

def get_service_links_date(startdate):
    get_service()

    rep_keys = ['id', 'startdate']
    tr_list = driver.driver.find_elements_by_tag_name("tr")
    links = []

    for i in range(1, len(tr_list)):
        data = {}
        param_startdate = parse_date(startdate)
        for key in rep_keys:
            rep_name = 'rep_' + key + str(i)
            value = driver.find_name(rep_name).get_attribute("value")
            data[key] = value

        if (parse_date(data['startdate']) >= param_startdate):
            links.append(data)

    return links

def fill_form(index):
    citizen_index = random.randint(0, len(citizens) - 1)
    repair_index = random.randint(0, len(repairs) - 1)

    citizen = citizens[citizen_index]
    repair = repairs[repair_index]
    item = repair['item']
    item_type = item['item_type']
    start_date = repair['created_at'].split(' ')[0]
    start_time = repair['created_at'].split(' ')[1]

    driver.find_id("ser_idcard").send_keys(citizen['number'])
    driver.find_id("ser_name").send_keys(citizen['title'] + citizen['fullname'])
    driver.find_id("ser_address").send_keys(address(citizen['address']))
    driver.find_id("ser_code").send_keys(index + 1)
    driver.find_id("ser_time").send_keys(start_time)
    driver.find_id("ser_phone").send_keys(citizen['phonenumber'])
    startdate = 'document.getElementById("ser_startdate").value = arguments[0];'
    driver.driver.execute_script(startdate, start_date)
    select = driver.select_name("equ_id")
    select.select_by_value(item_type['code'])

    driver.find_id("rep_cause").send_keys(item['problem'])
    driver.find_id("rep_performance").send_keys(repair['description'])

    select = driver.select_name("ser_result")
    select.select_by_value("ซ่อมได้")

    select = driver.select_name("ser_standby")
    select.select_by_value("รับแล้ว")

    driver.find_id("ser_idcard").send_keys(Keys.ENTER)
    driver.wait_alert()

    return

def insert(service_link):
    rand = random.randint(0, 6)

    for i in range(0, rand):
        # driver.get(service_link)
        driver.service(service_link['id'])
        # driver.wait_url("service.php")
        fill_form(i)

    return

def repairs(fixit_cen_id, fixit_date):
    repairs = requests.get('http://fixitchainat.ddns.net:8000/api/repairs/notwait/' + fixit_cen_id + '/' + fixit_date)
    return repairs.json()['data']

if __name__ == "__main__":
    citizens = openfile('citizens.json')
    citizens = citizens[2]['data']
    repairs = openfile('repairs.json')

    driver = sd.Driver()
    driver.login()

    # service_links = get_service_links()
    service_links = get_service_links_date("2019-05-13")

    for i in range(1, len(service_links)):
        insert(service_links[i])
