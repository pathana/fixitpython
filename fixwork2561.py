import json
from modules import driver as sd
from selenium.webdriver.common.keys import Keys

driver = None
cen_id = "820"
rep_id = "2524"

def openfile(name):
    with open(name, 'r', encoding='utf8') as f:
        data = json.load(f)

    return data

def item_type(name):
    value = {
        "เครื่องปั่น": "311",
        "พัดลม": "301",
        "มอเตอร์ไซด์": "101",
        "โทรทัศน์": "305",
        "ไมโครเวฟ": "311",
        "จักรยาน": "106",
        "สายไฟ": "311",
        "หม้อหุ้งข้าว": "302",
        "เครื่องเล่น  CD": "309",
        "ฝาหม้ออบ": "307",
        "มอไซค์": "101",
        "เครื่องเก็บไฟ": "311",
        "หม้อทำขนม": "307",
        "วิทยุ": "309",
        "เครืองนวด": "311",
        "เครื่องซักผ้า": "308",
        "รถจักรยาน": "106",
        "เครื่องเล่น DvD": "309",
        "เครื่องแปลไฟ": "311",
        "เตครื่องขยาย": "309",
        "เครื่องตัดหญ้า": "203",
        "เตารีด": "303",
        "จักรยานยนต์": "101",
        "ตู้เย็น": "306",
        "ยางใน": "101"
    }

    try:
        return value[name]
    except:
        return "311"

def fullname(work):
    return work['Cus_Sex'] + work['Cus_Name'] + "  " + work['Cus_Surename']

def address(work):
    address = (work['Number'] + " " +
            "หมู่ " + work['section'] +
            " ต." + work['district'] + " อ." + work['canton'] +
            " จ." + work['province'] + " " + work['zipcode'])
    return address

def result(work):
    value = {
        "ได้": "ซ่อมได้",
        "ไม่ได้": "ซ่อมไม่ได้",
        "": "ซ่อมไม่ได้"
    }

    return value[work['work_status']]

def description(work):
    if work['work_win'] == "":
        return "-"

    return work['work_win']

def problem(work):
    if work['work_lose'] == "":
        return "-"

    return work['work_lose']

def get_service():
    driver.center()
    driver.repair(cen_id)
    driver.service(rep_id)

def insert(work, index):
    driver.find_id("ser_idcard").send_keys(work['Cus_Id'])
    driver.find_id("ser_name").send_keys(fullname(work))
    driver.find_id("ser_address").send_keys(address(work))
    driver.find_id("ser_code").send_keys(index + 1)
    startdate = 'document.getElementById("ser_startdate").value = arguments[0];'
    driver.driver.execute_script(startdate, work['work_date'])
    driver.find_id("ser_time").send_keys(work["work_addtime"])
    driver.find_id("ser_phone").send_keys(work["Cus_Tel"])
    select = driver.select_name("equ_id")
    select.select_by_value(item_type(work['work_type']))
    driver.find_id("rep_cause").send_keys(problem(work))
    driver.find_id("rep_performance").send_keys(description(work))

    select = driver.select_name("ser_result")
    select.select_by_value(result(work))

    select = driver.select_name("ser_standby")
    select.select_by_value("รับแล้ว")

    driver.find_id("ser_idcard").send_keys(Keys.ENTER)
    driver.wait_alert()

if __name__ == "__main__":
    works = openfile('work.json')
    works = works[2]['data']
    driver = sd.Driver()
    driver.login()

    for i in range(11, len(works)):
        work = works[i]
        get_service()
        insert(work, i)
