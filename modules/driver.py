from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select, WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class Driver:
    def __init__(self):
        download_dir = "C:\\Users\\lenovo-v530\\Documents"
        options = webdriver.ChromeOptions()
        profile = {
            "plugins.plugins_list": [
                {
                    "enabled": False,
                    "name": "Chrome PDF Viewer"
                }
            ],
            "download.default_directory": download_dir,
            "download.extensions_to_open": "applications/pdf"
        }
        options.add_experimental_option("prefs", profile)
        self.driver = webdriver.Chrome(chrome_options=options)
        self.url = "http://reward.svc.ac.th/fixit62/"
        self.username = "1318016101"
        self.password = "chananwan"

    def find_id(self, name):
        return self.driver.find_element_by_id(name)

    def find_name(self, name):
        return self.driver.find_element_by_name(name)

    def get(self, name):
        return self.driver.get(name)

    def urlPath(self, name):
        return self.url + name

    def select_name(self, name):
        return Select(self.find_name(name))

    def wait_alert(self):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.alert_is_present())
        alert = self.driver.switch_to_alert()
        alert.accept()
        return

    def wait_url(self, name):
        wait = WebDriverWait(self.driver, 10)
        wait.until(EC.url_matches(self.urlPath(name)))
        return

    def login(self):
        self.get(self.urlPath("login.php"))
        self.find_id("mem_username").send_keys(self.username)
        self.find_id("mem_password").send_keys(self.password)
        self.find_id("mem_password").send_keys(Keys.ENTER)
        self.wait_url("index.php")
        return

    def center(self):
        self.get(self.urlPath("index.php?action=center"))
        self.wait_url("center.php")
        return

    def print_service_pdf(self, id, dt):
        self.get(self.urlPath("repairdate_printservicepdf.php?token=&rep_id=" + str(id) + "&rep_startdate=" + str(dt)))
        return

    def repair(self, id):
        self.get(self.urlPath("center.php?action=repairs&cen_id=" + str(id)))
        self.wait_url("repair.php")
        return

    def service(self, id):
        self.get(self.urlPath("repair.php?action=service&rep_id=" + str(id)))
        self.wait_url("service.php")
        return

    def repairmember(self, id):
        self.get(self.urlPath("repair.php?action=repairmember&rep_id=" + str(id)))
        self.wait_url("repair_member.php")
        return

    def servicelist(self, id):
        self.get(self.urlPath("repair.php?action=servicelist&rep_id=" + str(id)))
        self.wait_url("service_list.php")
        return
