from modules import driver as sd
from selenium.webdriver.common.keys import Keys

driver = None
cen_id = "820"
rep_id = "2323"

def get_service():
    driver.center()
    driver.repair(cen_id)
    driver.service(rep_id)

def insert():
    driver.find_id("ser_idcard").send_keys("1149900301894")
    driver.find_id("ser_name").send_keys("name surename")
    driver.find_id("ser_address").send_keys("address")
    driver.find_id("ser_code").send_keys("1")
    startdate = 'document.getElementById("ser_startdate").value = arguments[0];'
    driver.driver.execute_script(startdate, "2019-05-25")
    driver.find_id("ser_time").send_keys("12:35")
    driver.find_id("ser_phone").send_keys("0841499694")
    select = driver.select_name("equ_id")
    select.select_by_value("205")
    driver.find_id("rep_cause").send_keys("problem issue")
    driver.find_id("rep_performance").send_keys("problem description")

    select = driver.select_name("ser_result")
    select.select_by_value("ระบุผลการซ่อม")

    select = driver.select_name("ser_standby")
    select.select_by_value("รอรับ")

    driver.find_id("ser_idcard").send_keys(Keys.ENTER)
    driver.wait_alert()

if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    get_service()
    insert()
