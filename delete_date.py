from modules import driver as sd
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
import requests

driver = None
cen_id = "820"
rep_id = "2524"

def get_service():
    driver.center()
    driver.repair(cen_id)

def delete_repair_member():
    driver.repairmember(rep_id)
    delete_links = []

    links = driver.driver.find_elements(By.PARTIAL_LINK_TEXT, "ลบ")
    for link in links:
        delete_links.append(link.get_attribute("href"))

    for delete in delete_links:
        driver.get(delete)

def delete_service_list():
    driver.servicelist(rep_id)
    delete_links = []

    links = driver.driver.find_elements(By.PARTIAL_LINK_TEXT, "ลบข้อมูล")
    for link in links:
        delete_links.append(link.get_attribute("href"))

    for delete in delete_links:
        driver.get(delete)


if __name__ == "__main__":
    driver = sd.Driver()
    driver.login()
    get_service()
    # delete_repair_member()
    delete_service_list()
